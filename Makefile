CONFIG_FILE?=samconfig.toml
CONFIG_ENV?=default
AWS_PROFILE?=default
PACKAGE?=

all: build deploy

build:
	cd packages/${PACKAGE} && sam build

deploy:
	cd packages/${PACKAGE} && sam deploy --config-file=${CONFIG_FILE} --config-env=${CONFIG_ENV} --profile=${AWS_PROFILE}
