# AWS & Customer.io

Как работает СтепФункция **CustomerIoWorkflow** - получая на вход данные пользователя (email, phone, name, event и тд.) СтепФункция выполняет шаги по обработке этих данных и созданию/обновлению информации в customer.io

Помимо этого СтепФункция избыточна на создание **event'ов** для конертного пользователя customer.io

В `workflow.asl.json` находится описание самой СтепФункции, кратко - по входным параметрам определяется - пойдет ли создание/обновление в customer.io используя lms пользователя, либо в случае несуществования - по входным данным из формы

## Required & Optional payload

```json
{
  "last_login": 1656712250000, // Optional
  "lang_code": "es-MX", // Required
  "user": {
    // Required (Optional if lms_user provided)
    "email.$": "$.input.email", // Required, if user provided!
    "full_name_via_form.$": "$.input.name", // Optional
    "phone.$": "$.input.phone", // Optional
    "status": "lead" // Optional
  },
  "lms_user": {
    // Required (Optional if user provided)
    "email.$": "$.input.email" // Required, if lms_user provided!
    /** LMS User Entity supported **/
  },
  "variables": {
    // Required
    "cioTrackDomain": "track-eu.customer.io", // Required
    "cioAppDomain": "api-eu.customer.io", // Required
    "lmsApiUrl": "${lmsApiUrl}", // Required
    "lmsApiKey": "${lmsApiKey}", // Required
    "customerIoSiteId": "${customerIoSiteId}", // Required
    "customerIoApiKey": "${customerIoApiKey}", // Required
    "customerIoToken": "${customerIoToken}" // Required
  },
  "cio_event": {
    // Optional
    /** Customer.io events supported **/
  }
}
```

## Explore directories & files

`./lambdas/` - все используемые лямбды, customer.io

- `├──create-customer/` - [API] создание пипла в customer.io
- `├──create-customer-event/` - [API] создание event для пипла
- `├──dynamo-extract/` - вытаскивает из DynamoDB данные
- `├──is-customer-exists/` - [API] проверка на существование пипла
- `├──prepare-customer/` - подготовить атрибуты кастомера перед созданием
- `├──prepare-enrollments-event/` - подготовить enrollments event перед созданием
- `├──prepare-lms-user-to-customer/` - подготовить lms user в атрибуты кастомера перед созданием
- `├──prepare-shopping-cart-event/` - подготовить shopping-cart event перед созданием
- `└──prepare-webinar-register-event/` - подготовить webinar-register event перед созданием

`../lms/lambdas` - дополнительно используюстся лямбды lms

- `├──lms-get-enrollment/` - [API] lms получить enrollment по id
- `├──lms-get-user-actual-enrollment/` - вытащить самый актульный enrollment
- `├──lms-get-user-by-id/` - [API] - lms получить пользователя по id
- `├──lms-get-user-certs/` - [API] lms получить сертификаты пользователя
- `└──lms-search-user/` - [API] lms найти пользователя по email

`./layers/` - модели, интерфейсы, enum'ы

`./state-machines/` - степ функции

`./samconfig.toml` - конфиг билда на прод

`./template.yml` - AWS cloudformation template

## Building & Deploying

```sh
sam build --beta-features

sam deploy # sam deploy --profile prod если используете неск. профилей
```

## Troubleshooting

### Layers & TypeScript?

```js

```

### How to execute StepFunction out of StepFunction/Lambda?

```js

```
