import { strict } from "assert";
import { CIOEvent } from "/opt/customer/index";

interface ISubscription {
  options: IOptions;
  client: IClient;
  source: ISource;
  webinar: IWebinar;
  user: IUser;
  clientId: string;
  userId: string;
  sourceId: string;
  webinarId: string;
  externalId: string | null;
  reference: string | null;
  country: string | null;
  id: string;
  created: string;
  modified: string;
}

interface IOptions {}
interface IClient {}
interface ISource {}
interface IWebinar {}
interface IUser {}

export const handler = async ({
  subscription,
}: {
  subscription: ISubscription;
}): Promise<CIOEvent.IEvent> => {
  strict.ok(subscription, "event.subscription must be provided");
  return {
    name: "form_webinar_register",
    data: { subscription },
  };
};
