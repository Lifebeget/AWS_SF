import { strict } from "assert";
import { Customer } from "/opt/customer/index";
import { LMS } from "/opt/customer/index";

enum Mutation {
  CREATE = "create",
  UPDATE = "update",
}

export const handler = async ({
  last_lms_login,
  lms_user_enrollment,
  lms_user,
  lang_code,
  mutation,
}: {
  last_lms_login: number;
  lms_user_enrollment: LMS.ILmsUserEnrollment;
  lms_user: LMS.ILmsUser;
  lang_code: Customer.PossibleLangCodes;
  mutation: Mutation;
}): Promise<Customer.ICustomer> => {
  strict.ok(mutation, "event.mutation must be provided");
  strict.ok(lms_user, "event.lms_user must be provided");
  strict.ok(lms_user.email, "event.lms_user.email must be provided");
  if (
    !Object.values(Customer.PossibleLangCodes).includes(
      lang_code as Customer.PossibleLangCodes
    )
  ) {
    throw new Error(`event.lang_code must enum ${Customer.PossibleLangCodes}`);
  }

  const lms_user_status: Customer.UserStatuses = lms_user?.enrollments
    ? lms_user.enrollments.some((e) => e.active === true)
      ? Customer.UserStatuses.STUDENT
      : Customer.UserStatuses.NOT_STUDENT
    : Customer.UserStatuses.NOT_STUDENT;

  if (mutation == Mutation.CREATE)
    return {
      email: lms_user.email,
      phone: lms_user.profile?.phone,
      lms_id: lms_user.id,
      full_name: lms_user.name,
      first_name: lms_user.profile?.name,
      last_name: lms_user.profile?.surname,
      birthday: lms_user.profile?.birthdate,
      created_at_date: new Date().toLocaleDateString(
        lang_code || Customer.PossibleLangCodes.BRAZIL
      ),
      created_at_time: new Date().toLocaleTimeString(
        lang_code || Customer.PossibleLangCodes.BRAZIL
      ),
      created_at_ts: Date.now(),
      modified_at_date: new Date().toLocaleDateString(
        lang_code || Customer.PossibleLangCodes.BRAZIL
      ),
      modified_at_time: new Date().toLocaleTimeString(
        lang_code || Customer.PossibleLangCodes.BRAZIL
      ),
      modified_at_ts: Date.now(),
      last_lms_login,
      status: lms_user_status,
      actual_course_name: lms_user_enrollment?.course?.title,
      lang_code, // No column lang_code in LMS for now
      // vertical, // No vertical for now
    };

  if (mutation == Mutation.UPDATE)
    return {
      email: lms_user.email,
      phone: lms_user.profile?.phone,
      lms_id: lms_user.id,
      full_name: lms_user.name,
      first_name: lms_user.profile?.name,
      last_name: lms_user.profile?.surname,
      birthday: lms_user.profile?.birthdate,
      modified_at_date: new Date().toLocaleDateString(
        lang_code || Customer.PossibleLangCodes.BRAZIL
      ),
      modified_at_time: new Date().toLocaleTimeString(
        lang_code || Customer.PossibleLangCodes.BRAZIL
      ),
      modified_at_ts: Date.now(),
      last_lms_login,
      status: lms_user_status,
      actual_course_name: lms_user_enrollment?.course?.title,
      lang_code, // No column lang_code in LMS for now
      // vertical, // No vertical for now
    };

  throw new Error(
    `event.mutation must be ${Mutation.CREATE} | ${Mutation.UPDATE}`
  );
};
