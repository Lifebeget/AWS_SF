import { Customer } from "/opt/customer/index";
import axios from "axios";
import { strict } from "assert";

export const handler = async ({
  customer,
  cio_track_domain,
  cio_site_id,
  cio_api_key,
}: {
  customer: Customer.ICustomer;
  cio_track_domain: string;
  cio_site_id: string;
  cio_api_key: string;
}): Promise<any> => {
  strict.ok(cio_track_domain, "event.cio_track_domain must be provided");
  strict.ok(cio_site_id, "event.cio_site_id must be provided");
  strict.ok(cio_api_key, "event.cio_api_key must be provided");
  strict.ok(customer, "event.customer must be provided");
  strict.ok(customer.email, "event.customer.email must be provided");

  /** Create customer in customer.io */
  const res = await axios({
    /** We will user identifier as email */
    url: `https://${cio_track_domain}/api/v1/customers/${customer.email}`,
    method: "PUT",
    auth: {
      username: cio_site_id,
      password: cio_api_key,
    },
    data: customer,
  });

  return customer;
};
