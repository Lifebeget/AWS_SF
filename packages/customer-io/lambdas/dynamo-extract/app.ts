import { strict } from "assert";
import { DynamoDB } from "aws-sdk";

export const handler = async ({
  table_name,
  key,
}: {
  table_name: string;
  key: string;
}) => {
  strict.ok(table_name, "event.table_name must be provided");
  strict.ok(key, "event.key must be provided");
  /** DynamoDB or Secret storage variables */
  const dynamodb = new DynamoDB();
  const TableName = table_name;
  const Key = { name: { S: key } };
  const dynamo_record = await dynamodb.getItem({ TableName, Key }).promise();

  return dynamo_record;
};
