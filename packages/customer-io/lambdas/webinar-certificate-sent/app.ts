import axios from "axios";
import { strict } from "assert";
import * as qs from "qs";
import { CIOEvent } from "/opt/customer/index";
import { Customer } from "/opt/customer/index";

export const handler = async ({
  customer,
  webinar_cert,
  cio_track_domain,
  cio_site_id,
  cio_api_key,
}: {
  customer: Customer.ICustomer;
  webinar_cert: CIOEvent.IEvent;
  cio_track_domain: string;
  cio_site_id: string;
  cio_api_key: string;
}) => {
  strict.ok(cio_track_domain, "cio_track_domain required");
  strict.ok(cio_site_id, "cio_site_id required");
  strict.ok(cio_api_key, "cio_api_key required");
  try {
    await axios({
      url: `https://${cio_track_domain}/api/v1/customers/${customer.email}/events`,
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      auth: {
        username: cio_site_id,
        password: cio_api_key,
      },
      data: qs.stringify(webinar_cert),
    });
  } catch (error) {
    console.log(error);
  }

  return {
    customer,
    webinar_cert,
    cio_track_domain,
    cio_site_id,
    cio_api_key,
  };
};