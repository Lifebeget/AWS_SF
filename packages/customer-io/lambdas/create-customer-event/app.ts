import axios from "axios";
import { strict } from "assert";
import { CIOEvent } from "/opt/customer/index";
import { Customer } from "/opt/customer/index";

export const handler = async ({
  customer,
  customer_event,
  cio_track_domain,
  cio_site_id,
  cio_api_key,
}: {
  customer: Customer.ICustomer;
  customer_event: CIOEvent.IEvent;
  cio_track_domain: string;
  cio_site_id: string;
  cio_api_key: string;
}) => {
  /** Input params */

  strict.ok(customer.email, "event.customer.email must be provided");
  strict.ok(customer_event, "event.customer_event must be provided");
  strict.ok(cio_track_domain, "event.cio_track_domain must be provided");
  strict.ok(cio_site_id, "event.cio_site_id must be provided");
  strict.ok(cio_api_key, "event.cio_api_key must be provided");

  /** Create customer-event in customer.io */
  const res = await axios({
    /** We will user identifier as id */
    url: `https://${cio_track_domain}/api/v1/customers/${customer.email}/events`,
    method: "POST",
    auth: {
      username: cio_site_id,
      password: cio_api_key,
    },
    data: {
      name: customer_event.name,
      type: "event",
      data: customer_event.data,
    },
  });

  return customer_event;
};
