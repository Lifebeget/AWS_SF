import { strict } from "assert";
import { CIOEvent } from "/opt/customer/index";
import { Customer } from "/opt/customer/index";
/**
 * Warn! Current lambda require shoppingCart
 * Use /subscriptions lambdas to provide shoppingCart
 */

interface IShoppingCart {
  products: IProduct[];
}

interface IProduct {
  id: string;
  shortName: string;
}

export const handler = async ({
  shoppingCart,
  lang_code,
}: {
  shoppingCart: IShoppingCart;
  lang_code: Customer.PossibleLangCodes;
}): Promise<CIOEvent.IEvent> => {
  strict.ok(shoppingCart, "event.shoppingCart must be provided");
  strict.ok(lang_code, "event.lang_code must be provided");
  if (
    !Object.values(Customer.PossibleLangCodes).includes(
      lang_code as Customer.PossibleLangCodes
    )
  ) {
    throw new Error(`event.lang_code must enum ${Customer.PossibleLangCodes}`);
  }

  /**
   * Just return customer.io event
   * This obj will be sent as form_purchase_course event
   * via customer.io
   * ( see /customer-io/create-client-event )
   */
  return {
    name: "form_purchase_course",
    data: {
      products: shoppingCart.products.map((product) => ({
        status: "new",
        nomenclatureId: product.id,
        date: new Date().toLocaleDateString(
          lang_code || Customer.PossibleLangCodes.BRAZIL
        ),
        time: new Date().toLocaleTimeString(
          lang_code || Customer.PossibleLangCodes.BRAZIL
        ),
        ts: Date.now(),
        courseName: product.shortName,
      })),
    },
  };
};
