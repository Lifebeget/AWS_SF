import { strict } from "assert";
import { Customer } from "/opt/customer/index";

enum Mutation {
  CREATE = "create",
  UPDATE = "update",
}

const parseFormName = (
  form_full_name?: string
): { form_first_name?: string; form_last_name?: string } => {
  if (!form_full_name) {
    return {
      form_first_name: undefined,
      form_last_name: undefined,
    };
  }
  const [form_first_name, form_last_name] = form_full_name.split(" ");
  return {
    form_first_name,
    form_last_name,
  };
};

export const handler = async ({
  mutation,
  full_name_via_form,
  email,
  phone,
  lms_id,
  full_name,
  first_name,
  last_name,
  birthday,
  created_at_date,
  created_at_time,
  created_at_ts,
  modified_at_date,
  modified_at_time,
  modified_at_ts,
  last_lms_login,
  status,
  actual_course_name,
  vertical,
  lang_code,
}: {
  mutation: Mutation;
  full_name_via_form?: string;
  email: string;
  phone?: string;
  lms_id?: string;
  full_name?: string;
  first_name?: string;
  last_name?: string;
  birthday?: string;
  created_at_date?: string;
  created_at_time?: string;
  created_at_ts?: number;
  modified_at_date?: string;
  modified_at_time?: string;
  modified_at_ts?: number;
  last_lms_login?: string;
  status?: Customer.UserStatuses;
  actual_course_name?: string;
  vertical?: string;
  lang_code: Customer.PossibleLangCodes;
}): Promise<Customer.ICustomer> => {
  strict.ok(mutation, "event.mutation must be provided");
  strict.ok(email, "event.email must be provided");
  if (
    !Object.values(Customer.PossibleLangCodes).includes(
      lang_code as Customer.PossibleLangCodes
    )
  ) {
    throw new Error(`event.lang_code must enum ${Customer.PossibleLangCodes}`);
  }

  const { form_first_name, form_last_name } = parseFormName(full_name_via_form);

  if (mutation == Mutation.CREATE)
    return {
      email,
      phone,
      lms_id,
      full_name: full_name_via_form ? full_name_via_form : full_name,
      first_name: form_first_name ? form_first_name : first_name,
      last_name: form_last_name ? form_last_name : last_name,
      birthday,
      created_at_date: created_at_date
        ? created_at_date
        : new Date().toLocaleDateString(
            lang_code || Customer.PossibleLangCodes.BRAZIL
          ),
      created_at_time: created_at_time
        ? created_at_time
        : new Date().toLocaleTimeString(
            lang_code || Customer.PossibleLangCodes.BRAZIL
          ),
      created_at_ts: created_at_ts ? created_at_ts : Date.now(),
      modified_at_date: modified_at_date
        ? modified_at_date
        : new Date().toLocaleDateString(
            lang_code || Customer.PossibleLangCodes.BRAZIL
          ),
      modified_at_time: modified_at_time
        ? modified_at_time
        : new Date().toLocaleTimeString(
            lang_code || Customer.PossibleLangCodes.BRAZIL
          ),
      modified_at_ts: modified_at_ts ? modified_at_ts : Date.now(),
      last_lms_login,
      status,
      actual_course_name,
      vertical,
      lang_code,
    };
  if (mutation == Mutation.UPDATE)
    return {
      email,
      phone,
      lms_id,
      full_name,
      first_name,
      last_name,
      birthday,
      created_at_date,
      created_at_time,
      created_at_ts,
      modified_at_date: modified_at_date
        ? modified_at_date
        : new Date().toLocaleDateString(
            lang_code || Customer.PossibleLangCodes.BRAZIL
          ),
      modified_at_time: modified_at_time
        ? modified_at_time
        : new Date().toLocaleTimeString(
            lang_code || Customer.PossibleLangCodes.BRAZIL
          ),
      modified_at_ts: modified_at_ts ? modified_at_ts : Date.now(),
      last_lms_login,
      status,
      actual_course_name,
      vertical,
      lang_code,
    };

  throw new Error(
    `event.mutation must be ${Mutation.CREATE} | ${Mutation.UPDATE}`
  );
};
