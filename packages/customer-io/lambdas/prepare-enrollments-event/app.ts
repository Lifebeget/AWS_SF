import { strict } from "assert";
import { CIOEvent } from "/opt/customer/index";
import { LMS } from "/opt/customer/index";

/**
 * Warn! Current lambda require enrollment
 */
export const handler = ({
  enrollment,
}: {
  enrollment: LMS.ILmsUserEnrollment;
}): CIOEvent.IEvent => {
  /**
   * Proceed only EnrollmentType.Buy
   */
  strict.ok(enrollment);
  strict.ok(enrollment.type === LMS.LmsEnrollmentType.Buy);

  /**
   * Just return customer.io event
   * This obj will be sent as enrollment event
   * via customer.io
   * ( see /customer-io/create-customer-event )
   */
  return {
    name: "enrollment",
    data: {
      enrollment,
    },
  };
};
