import axios from "axios";
import { strict } from "assert";

export const handler = async ({
  email,
  cio_app_domain,
  cio_bearer,
}: {
  email: string;
  cio_app_domain: string;
  cio_bearer: string;
}): Promise<Boolean> => {
  strict.ok(cio_app_domain, "event.cio_app_domain must be provided");
  strict.ok(cio_bearer, "event.cio_bearer must be provided");
  strict.ok(email, "event.email must be provided");

  /** Create customer in customer.io */
  const res = await axios({
    /** We will user identifier as email */
    url: `https://${cio_app_domain}/v1/customers`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${cio_bearer}`,
    },
    params: {
      email,
    },
  });

  return Boolean(res.data.results.length);
};
