export interface ILmsUser {
  id: string;
  name: string;
  email: string;
  active: boolean;
  certs: ILmsUserCert[];
  profile: ILmsUserProfile | null;
  enrollments: ILmsUserEnrollment[];
}

export interface ILmsUserCert {
  id: string;
  uniqueCertId: string;
  template?: string;
  created?: string;
  modified?: string;
  course?: ILmsCourse;
  webinar?: ILmsWebinar;
}

export interface ILmsWebinar {
  id: string;
  externalId: number;
  slug: string;
  name: string;
  type: string;
  additional: string;
  subtitle: string;
  startDateUtc?: string;
  startDateText: string;
  active?: boolean;
  finished?: boolean;
  pageUrl?: string;
  pageUrlOriginal: string | null;
  speakerBiography?: string;
  description: string;
  directionId: number;
  youtubePlaylistId: number;
  recordLink: string;
  created: string;
  modified: string;
  deleted: string;
  rev: number;
  published?: boolean;
  finalStartDateUtc?: string;
  certTemplate?: string;
  certTemplateData?: string;
  surveyId: string;
  surveyOpenDate?: string;
  surveyCloseDate?: string;
  public: boolean;
}

export interface ILmsCourse {
  id: string;
  externalId: string;
  cmsId: string;
  sort: number;
  slug: string;
  title: string;
  description?: string;
  active?: boolean;
  created?: string;
  modified?: string;
  showLink?: boolean;
  published?: boolean;
  publishedAt?: string;
  type?: LmsCourseType;
  duration?: number;
  version?: number;
  color?: string;
  certTemplate?: string;
  certTemplateData?: string;
  numberModulesFromZero: boolean;
  autoNumbering: boolean;
  supportDemand: number;
  status: LmsCourseStatus;
  startAt?: string;
  isAutoEnroll: boolean;
  notForSale: boolean;
  isForumEnabled: boolean;
  isSatellite: boolean;
  socialGroupLink?: string;
  expModules?: number;
  expLectures?: number;
  expQuizzes?: number;
}

export enum LmsCourseType {
  course = "course",
  profession = "profession",
}

export enum LmsCourseStatus {
  draft = "draft",
  new = "new",
  created = "created",
  uncompleted = "uncompleted",
  completed = "completed",
}

export enum LmsCourseRole {
  Student = "student",
  Tutor = "tutor",
  Moderator = "moderator",
  Support = "support",
  Teamlead = "teamlead",
}

export enum LmsEnrollmentType {
  Buy = "buy", // курс куплен
  Present = "present", // курс предоставлен в подарок к другому курсу
  Promo = "promo", // бесплатная подписка для разных промо-акций
  Inner = "inner", // внутренняя подписка для сотрудников
  Satellite = "satellite", // курс является дополнениемм к другому курсу
  Transfer = "transfer", // подписка переведена с другого курса
  Trial = "trial", // пробная подписка
  Selflearn = "selflearn", // пробная подписка на весь курс но без возможности отправлять домашки
  Test = "test", // тестовая подписка
  Unknown = "unknown", // неизвестно
}

export interface ILmsUserEnrollment {
  id: string;
  externalId: string;
  userId: string;
  course: ILmsCourse;
  courseId: string;
  role: LmsCourseRole;
  type: LmsEnrollmentType;
  active?: boolean;
  dateFrom?: string;
  dateTo?: string;
  created: string;
  modified?: string;
  graduatedAt?: string;
  reason?: Record<string, unknown>;
  interactiveTariff?: number;
  teamleadFee?: number;
  options?: Record<string, unknown>;
}

export interface ILmsUserProfile {
  userId: string;
  name?: string;
  surname?: string;
  birthdate?: string;
  cpf?: string;
  city?: string;
  phone?: string;
  phoneClean?: string;
  data?: string;
}
