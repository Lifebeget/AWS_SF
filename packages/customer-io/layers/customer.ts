/**
 * Customer.io customer model
 */
export interface ICustomer {
  email: string;
  phone?: string;
  lms_id?: string;
  full_name?: string;
  first_name?: string;
  last_name?: string;
  birthday?: string;
  created_at_date?: string;
  created_at_time?: string;
  created_at_ts?: number;
  modified_at_date?: string;
  modified_at_time?: string;
  modified_at_ts?: number;
  last_lms_login?: string;
  status?: UserStatuses;
  actual_course_name?: string; // Last enrollment with 'buy' type
  vertical?: string; // ?Possible enum in the future?
  lang_code: PossibleLangCodes;
}

/**
 * STUDENT - IF lms user AND active enrollment
 * NOT_STUDENT - IF lms user
 * LEAD - IF none of previous
 */
export enum UserStatuses {
  STUDENT = "student",
  NOT_STUDENT = "not_student",
  LEAD = "lead",
}

/**
 * Use LangCodes for dates in JS/TS
 */
export enum PossibleLangCodes {
  BRAZIL = "pt-BR", // Portuguese Brazil
  MEXICO = "es-MX", // Spanish Mexico
  COLUMBIA = "es-CO", // Spanish Columbian  
  PERU = "es-PE", // Spanish Peru
  USA = "en-US", // English (United States)
}
