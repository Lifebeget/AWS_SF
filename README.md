# AWS Step Functions

## Requirements

- [AWS SAM CLI](https://aws.amazon.com/serverless/sam/)

## Packages

Each deployed stack should have its own package in `packages/` directory.
To create a new package, create its subdirectory, `template.yml` with required resources, and, optionally, some lambdas, step-functions, etc. See directory structure examples in `packages/subscriptions`.

Use scripts to build/deploy a new package.

Put common resources (reusable lambdas, etc) in `./common` directory and reference them as `../../common/lambdas/my-lambda` in package's `template.yml`.

## Scripts

`my-package` should be a package name from `packages/` directory

```
# Build & deploy a package
make PACKAGE=my-package

# Build only
make build PACKAGE=my-package

# Deploy only
make deploy PACKAGE=my-package
```

## Инструкции
### Как создать простую lambda функцию
Для начала, нам необходимо установить [Docker], [AWS CLI] и [AWS SAM CLI]
В AWS CLI необходимо провести базовую конфигурацию командой `aws configure`. В ней вводим ключи доступа и регион `sa-east-1` (формат вывода оставляем нетронутым или меняем по желанию)
Далее пушим себе репозиторий [AWS Step Function], в папке packages вводим комананду `sam init`, выбираем пункт `AWS Quick Start Template` и для создания простейшей Lambda функции нам хватит темплейта `Standalone function`. Среду выполнение, обычно выбираем `nodejs14.x`, `X-Ray` трейсинг не нужен, название пишем исходя из того, что делает функция.
Файлы `.gitignore`, `buildspec.yml` и подкаталог `__test__` удаляем.
Сама Labmda фукнция храниться в `src/handlers/hello-from-lambda.js`. Данный файл можно переименовать и переместить по необходимости в переделах созданного кататлога. Главное указать путь в файле `template.yaml`(об этом чуть позже).
Внутри асинхронной функции `exports.nameHandler`(где `nameHandler` так же можно изменить по желанию) необходимо написать рабочий код.

#### Файл `template.yaml`
В файле `template.yaml` указываются все параметры, политики и зависимости которые необходимы для работы ресурса(в данном случае Lambda-функции). В среднем, он выглядит так(что не хватает, можно добавить):
```yaml
Resources:
  helloFromLambdaFunction: # название можно менять
    Type: AWS::Serverless::Function
    Properties:
      Handler: src/handlers/hello-from-lambda.helloFromLambdaHandler # путь до файла и его название его обработчика тоже
      Runtime: nodejs14.x
      Architectures:
        - x86_64
      MemorySize: 128
      Timeout: 100
      Description: A Lambda function that returns a static string. #описание вот тоже можно изменить
      Environment: 
        Variables: #переменные окружающей среды(о них ниже)
          nameValue1: 'value1'
          nameValue2: 'value2'
      Policies: #политики и их параметры - запрос доступов к другим службам(о них тоже ниже)
        - DynamoDBCrudPolicy: # название политики
            TableName: "nameTableDynamoDB" ## и её параметры
        - AWSSecretsManagerGetSecretValuePolicy:
            SecretArn: 'abracadabra'
```
##### Environment Variables
Введенные перменные можно получить с через proccess.env
```js
const { nameValue1, nameValue2 } = process.env;
```
##### Политики
Для работы с некоторыми службами AWS(типо DynamoDB и SecretManager) необходимо прописать политику использования.

### Проверка работоспособности
Для запуска Lambda-функции локально, необходимо сначала её забилдить командой `sam build`. Далее, если всё успешно скомпилировалось, ввести команду `sam local invoke`, которая запустит Lambda функцию локально.

### Деплой
С помошью команды `sam deploy` можно задеплоить функцию. Если это происходит в первый раз, лучше всего запустить эту команду с параметром `--guided`, который поможет сформировать необхоимый файл конфигурации `samconfig.toml`. Необходимо будет указать имя, может быть регион, а все остальное без изменений.

### Как дебажить Lambda функцию в VS Code
1. Установить расширение [AWS Toolkit]
2. Создать в нём профиль
3. Перейти в файл с функцией, которую необходимо дебажить
4. Над ней должна появиться срочка `AWS: Add Debug Configuration`. Нажать на неё.
5. В выподающем меню выбрать необходимую среду выполнения. После этого сгенерируется файл `.vscode/launch.json`
6. Раставить точки остановки и нажать F5
7. Дебажить пока не найдем ошибку :D
ВАЖНО: при изменении названии функции или обработчика, а так же пути файла, это необходимо зафиксировать в файле `.vscode/launch.json`(или проще его удалить и сгенерировать конфигурацию дебагера заново)

[AWS CLI]: <https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html>
[AWS SAM CLI]: <https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html>
[Docker]: <https://docs.docker.com/engine/install/>
[AWS Step Function]: <https://gitlab.ebacdev.com/ebac-online/aws-step-functions/-/tree/main/>
[AWS Toolkit]: <https://aws.amazon.com/ru/visualstudiocode/>